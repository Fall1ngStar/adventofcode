package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readDataFromFile() []string {
	data, err := ioutil.ReadFile("input")
	check(err)
	return strings.Split(string(data), "\r\n")
}

func getValue(str string) int {
	val, err := strconv.Atoi(str)
	if err != nil {
		return registers[str]
	}
	return val
}

func processData(data []string) {
	movingIndex := 0
	for movingIndex < len(data) {
		fields := strings.Fields(data[movingIndex])
		switch fields[0] {
		case "cpy":
			registers[fields[2]] = getValue(fields[1])
		case "jnz":
			if getValue(fields[1]) != 0 {
				val, _ := strconv.Atoi(fields[2])
				movingIndex += val - 1
			}
		case "inc":
			registers[fields[1]]++
		case "dec":
			registers[fields[1]]--
		}
		movingIndex++
	}
}

var registers = make(map[string]int)

func main() {
	data := readDataFromFile()
	data = data[:len(data)-1]
	registers["c"] = 1
	processData(data)
	fmt.Println(registers)
}
