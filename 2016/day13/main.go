package main

import (
	"fmt"
	"math"
	"math/bits"
	"os"
	"strconv"
)

//Coord Struct
type Coord struct {
	x, y uint
}

func getNumber(x, y, fav uint) uint {
	var result = x*x + 3*x + 2*x*y + y + y*y + fav
	return uint(bits.OnesCount(result))
}

func getFav() uint {
	args := os.Args[1:]
	if len(args) < 1 {
		panic("wrong argument number")
	}
	val, err := strconv.Atoi(args[0])
	if err != nil {
		panic(err)
	}
	return uint(val)
}

func generateMatrix() Matrix {
	fav := getFav()
	matrix := make([][]uint, 100)
	for i := range matrix {
		matrix[i] = make([]uint, 100)
	}
	for x := range matrix {
		for y := range matrix[x] {
			matrix[x][y] = getNumber(uint(x), uint(y), fav) % 2
		}
	}
	return matrix
}

func displayMatrix(matrix [][]uint) {
	for _, line := range matrix {
		for _, val := range line {
			if val == 1 {
				fmt.Print("█")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}

//Matrix type
type Matrix [][]uint

//CoordSet type
type CoordSet []Coord

func (matrix Matrix) astar(start, goal Coord) int {
	closedSet := make(CoordSet, 0)
	openSet := make(CoordSet, 0)
	openSet = append(openSet, start)
	cameFrom := make(map[Coord]Coord)
	gScore := make(map[Coord]uint)
	for x := range matrix {
		for y := range matrix[x] {
			gScore[Coord{uint(x), uint(y)}] = 1<<32 - 1
		}
	}
	gScore[start] = 0
	fScore := make(map[Coord]uint)
	for x := range matrix {
		for y := range matrix[x] {
			fScore[Coord{uint(x), uint(y)}] = 1<<32 - 1
		}
	}
	fScore[start] = heuristic(start, goal)
	// fmt.Println("openSet : ", openSet)
	for len(openSet) > 0 {
		current, index := openSet.lowestFscore(fScore)
		// fmt.Println("current : ", current, ", goal : ", goal)
		if current == goal {
			// fmt.Println("you're arrived. gScore : ", gScore[goal], ", fScore : ", fScore[goal])
			return int(fScore[goal])
		}
		if len(openSet) > 1 {
			openSet = append(openSet[:index], openSet[index+1:]...)
		} else {
			openSet = nil
		}
		closedSet = append(closedSet, current)
		// fmt.Println("closedSet : ", closedSet)
		neighbours := matrix.getNeighbours(current)
		// fmt.Println(current, " got ", len(neighbours), " neighbours : ", neighbours)
		for _, neighbour := range neighbours {
			if !closedSet.contains(neighbour) {
				if !openSet.contains(neighbour) {
					openSet = append(openSet, neighbour)
				}
				tempGScore := gScore[current] + 1 //TODO: check with heuristic
				if tempGScore < gScore[neighbour] {
					// fmt.Println("tempGScore : ", tempGScore)
					cameFrom[neighbour] = current
					gScore[neighbour] = tempGScore
					// fmt.Println("gScore[", neighbour, "] : ", gScore[neighbour])
					// fmt.Println("heuristic(", neighbour, ",", goal, ") : ", heuristic(neighbour, goal))
					fScore[neighbour] = gScore[neighbour] + heuristic(neighbour, goal)
					// fmt.Println("fScore[", neighbour, "] : ", fScore[neighbour])
				}
			}
		}
		// fmt.Println("openSet : ", openSet)
	}
	return -1
}

func (matrix Matrix) getNeighbours(coord Coord) CoordSet {
	var result CoordSet
	if coord.x > 0 && matrix[coord.x-1][coord.y] == 0 {
		result = append(result, Coord{coord.x - 1, coord.y})
	}
	if coord.x < 99 && matrix[coord.x+1][coord.y] == 0 {
		result = append(result, Coord{coord.x + 1, coord.y})
	}
	if coord.y > 0 && matrix[coord.x][coord.y-1] == 0 {
		result = append(result, Coord{coord.x, coord.y - 1})
	}
	if coord.y < 99 && matrix[coord.x][coord.y+1] == 0 {
		result = append(result, Coord{coord.x, coord.y + 1})
	}
	// fmt.Println(coord, " : ", result)
	return result
}

func (set CoordSet) lowestFscore(fScore map[Coord]uint) (Coord, int) {
	var min Coord
	var value uint = 1<<32 - 1
	index := -1
	for i, coord := range set {
		if fScore[coord] < value {
			min = coord
			value = fScore[coord]
			index = i
		}
	}
	return min, index
}

func (set CoordSet) contains(coord Coord) bool {
	for _, val := range set {
		if val == coord {
			return true
		}
	}
	return false
}

func heuristic(n, goal Coord) uint {
	return uint(math.Abs(float64(int(n.x)-int(goal.x))) + math.Abs(float64(int(n.y)-int(goal.y))))
}

func main() {
	matrix := generateMatrix()
	fmt.Println(matrix.astar(Coord{1, 1}, Coord{31, 39}))
	sum := 0
	for x := 0; x < 100; x++ {
		for y := 0; y < 100; y++ {
			if matrix[x][y] == 1 {
				continue
			}
			dist := matrix.astar(Coord{1, 1}, Coord{uint(x), uint(y)})
			if dist > -1 && dist <= 50 {
				sum++
			}
		}
	}
	fmt.Println(sum)
	//displayMatrix(matrix)
}
