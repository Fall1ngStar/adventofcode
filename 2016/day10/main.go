package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

//Direction struct
type Direction struct {
	t, value string
}

//Bot struct
type Bot struct {
	values    []string
	name      string
	low, high Direction
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readDataFromFile() []string {
	data, err := ioutil.ReadFile("input")
	check(err)
	content := string(data)
	arr := strings.Split(content, "\n")
	return arr[:len(arr)-1]
}

func (b *Bot) addValue(value string) {
	b.values = append(b.values, value)
}

func min(a, b int) string {
	if b < a {
		a = b
	}
	return strconv.Itoa(a)
}

func max(a, b int) string {
	if b > a {
		a = b
	}
	return strconv.Itoa(a)
}

func (b *Bot) checkForValues() {
	if len(b.values) == 2 {
		val1, _ := strconv.Atoi(b.values[0])
		val2, _ := strconv.Atoi(b.values[1])
		fmt.Println(val1, val2, b)
		b.values = nil
		if b.low.t == "bot" {
			bots[b.low.value].addValue(min(val1, val2))
			bots[b.low.value].checkForValues()
		} else {
			outputs[b.low.value] = min(val1, val2)
		}
		if b.high.t == "bot" {
			bots[b.high.value].addValue(max(val1, val2))
			bots[b.high.value].checkForValues()
		} else {
			outputs[b.high.value] = max(val1, val2)
		}
	}
}

var bots = make(map[string]*Bot)
var outputs = make(map[string]string)

func process(data []string) {
	for _, line := range data {
		keys := strings.Fields(line)
		if keys[0] == "bot" {
			//fmt.Println(keys[1])
			bots[keys[1]] = &Bot{
				nil,
				keys[1],
				Direction{keys[5], keys[6]},
				Direction{keys[10], keys[11]}}
			//fmt.Println(bots[keys[1]])
		}
	}
	for _, line := range data {
		keys := strings.Fields(line)
		if keys[0] == "value" {
			_, ok := bots[keys[5]]
			if ok {
				bots[keys[5]].addValue(keys[1])
				bots[keys[5]].checkForValues()
			} else {
				bots[keys[5]] = &Bot{nil, keys[5], Direction{}, Direction{}}
				bots[keys[5]].addValue(keys[1])
				bots[keys[5]].checkForValues()
			}
		}
	}
}

func display() {
	for _, val := range bots {
		fmt.Println(val)
	}
}

func main() {
	content := readDataFromFile()
	process(content)
	//display()
	fmt.Println(outputs)
	fmt.Println(outputs["0"], " ", outputs["1"], " ", outputs["2"])
}
