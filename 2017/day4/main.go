package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

func readDataFromFile() [][]string {
	data, _ := ioutil.ReadFile("input")
	lines := strings.Split(string(data), "\r\n")
	lines = lines[:len(lines)-1]
	words := make([][]string, 0)
	for _, line := range lines {
		words = append(words, strings.Fields(line))
	}
	return words
}

func part1(data [][]string) {
	valids := 0
	for _, line := range data {
		contains := make(map[string]bool)
		valid := true
		for _, word := range line {
			if _, ok := contains[word]; ok {
				//fmt.Println("Duplicate ", word, " at line ", i+1)
				valid = false
			}
			contains[word] = true
		}
		if valid {
			valids++
		}
	}
	fmt.Println(valids)
}

func part2(data [][]string) {
	valids := 0
	for _, line := range data {
		contains := make(map[string]bool)
		valid := true
		for _, word := range line {
			word = toOrdererChars(word)
			if _, ok := contains[word]; ok {
				//fmt.Println("Duplicate ", word, " at line ", i+1)
				valid = false
			}
			contains[word] = true
		}
		if valid {
			valids++
		}
	}
	fmt.Println(valids)
}

//ByByte type
type ByByte []byte

func (a ByByte) Len() int           { return len(a) }
func (a ByByte) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByByte) Less(i, j int) bool { return a[i] < a[j] }

func toOrdererChars(str string) string {
	arr := ByByte(str)
	sort.Sort(arr)
	return string(arr)
}

func main() {
	data := readDataFromFile()
	part1(data)
	part2(data)
}
