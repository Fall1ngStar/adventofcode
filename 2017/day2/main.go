package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func readDataFromFile() [][]int {
	file, _ := ioutil.ReadFile("input")
	content := string(file)
	lines := strings.Split(content, "\n")
	fields := make([][]string, len(lines))
	for i := 0; i < len(lines); i++ {
		fields[i] = strings.Fields(lines[i])
	}
	result := make([][]int, len(fields))
	for i := 0; i < len(fields); i++ {
		result[i] = make([]int, len(fields[i]))
		for j := 0; j < len(fields[i]); j++ {
			val, _ := strconv.Atoi(fields[i][j])
			result[i][j] = val
		}
	}
	return result
}
func part1(data [][]int) {
	sum := 0
	for _, line := range data {
		sum += max(line...) - min(line...)
	}
	fmt.Println(sum)
}

func part2(data [][]int) {
	sum := 0
	for i, line := range data {
		for _, val1 := range line {
			for _, val2 := range line {
				if val1 != val2 {
					if val1%val2 == 0 {
						sum += val1 / val2
						fmt.Println("matching ", val1, " and ", val2, " on line ", i)
					}
					if val2%val1 == 0 {
						sum += val2 / val1
						fmt.Println("matching ", val1, " and ", val2, " on line ", i)
					}
				}
			}
		}
	}
	fmt.Println(sum)
}

func max(values ...int) int {
	result := values[0]
	for _, value := range values {
		if value > result {
			result = value
		}
	}
	return result
}

func min(values ...int) int {
	result := values[0]
	for _, value := range values {
		if value < result {
			result = value
		}
	}
	return result
}

func main() {
	fields := readDataFromFile()
	// fmt.Println(fields)
	part1(fields)
	part2(fields)
}
