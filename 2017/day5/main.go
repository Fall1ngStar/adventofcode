package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func readDataFromFile() []int {
	data, _ := ioutil.ReadFile("input")
	content := strings.Fields(string(data))
	values := make([]int, len(content))
	for i, str := range content {
		value, _ := strconv.Atoi(str)
		values[i] = value
	}
	return values
}

func part1(data []int) {
	instruction := 0
	index := 0
	for index < len(data) {
		curr := index
		index += data[index]
		data[curr]++
		instruction++
	}
	fmt.Println(instruction)
}

func part2(data []int) {
	instruction := 0
	index := 0
	for index < len(data) {
		curr := index
		index += data[index]
		if data[curr] >= 3 {
			data[curr]--
		} else {
			data[curr]++
		}
		instruction++
	}
	fmt.Println(instruction)
}

func main() {
	data := readDataFromFile()
	data2 := make([]int, len(data))
	copy(data2, data)
	part1(data)
	part2(data2)
}
