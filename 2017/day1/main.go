package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

func part1(content string) {
	sum := 0
	for i, num := range content {
		if content[i] == content[(i+1)%len(content)] {
			val, _ := strconv.Atoi(string(num))
			sum += val
		}
	}
	fmt.Println(sum)
}

func part2(content string) {
	sum := 0
	for i, num := range content {
		if content[i] == content[(i+(len(content)/2))%len(content)] {
			val, _ := strconv.Atoi(string(num))
			sum += val
		}
	}
	fmt.Println(sum)
}

func main() {
	data, _ := ioutil.ReadFile("input")
	content := string(data)
	part1(content)
	part2(content)
}
