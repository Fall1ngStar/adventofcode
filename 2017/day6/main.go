package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func readDataFromFile() []int {
	raw, _ := ioutil.ReadFile("input")
	data := strings.Fields(string(raw))
	result := make([]int, len(data))
	for i, dat := range data {
		val, _ := strconv.Atoi(dat)
		result[i] = val
	}
	return result
}

func part1(data []int) {
	count := 0
	steps := make(map[string]int)
	for {
		// fmt.Println(data)
		maxIndex := getMaxIndex(data)
		// fmt.Println("Current max index is ", maxIndex)
		toShare := data[maxIndex]
		data[maxIndex] = 0
		index := (maxIndex + 1) % len(data)
		for i := 0; i < toShare; i++ {
			data[index]++
			index = (index + 1) % len(data)
		}
		// fmt.Println("Shared ", toShare, " to all slots")
		count++
		str := IntArray(data).toString()
		// fmt.Println(str)
		if _, ok := steps[str]; ok {
			// fmt.Println("Duplicate step, infinite looping")
			fmt.Println(steps[str])
			break
		}
		steps[str] = count
		// fmt.Println()
	}
	fmt.Println(count)
}

func getMaxIndex(data []int) int {
	maxVal := data[0]
	maxIndex := 0
	for i, val := range data {
		if val > maxVal {
			maxIndex = i
			maxVal = val
		}
	}
	return maxIndex
}

//IntArray type
type IntArray []int

func (a IntArray) toString() string {
	result := ""
	for _, val := range a {
		result += strconv.Itoa(val) + " "
	}
	return result
}

func main() {
	data := readDataFromFile()
	part1(data)
}
